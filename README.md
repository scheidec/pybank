This is an example python package with functions simulating depositing and withdrawing cash from a bank account.

To install, clone the repository:

```
git clone https://gitlab.com/scheidec/pybank
```

and run the `setup.py` file from that directory:

```
python setup.py install
```
